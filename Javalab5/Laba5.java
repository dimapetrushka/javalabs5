package Javalab5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 12.03.2016.
 * 1)Создать в классе Circle метод, вычисляющий длину окружности
 * 2)Создать в классе Circle метод, перемещающий центр круга в случайную точку плоскости от [-99,-99] до [99,99]
 * 3)Поменять конструктор по умолчанию на конструктом, с передаваемыми в него, введенымми с клавиатуры числами
 * 4)Создать в классе Circle метод, вычисляющий расстояние между центрами двух окружностей
 * 5)Создать в классе Circle метод, проверяющий, касаются ли окружности в одной точке
 */
class Circle {
    //    создаем 4 переменных типа double
    public double x;
    public double y;
    public double r;
    public double d;

    //    создаем конструктор по умолчания с нулевыми координатами и единичным радиусом
    public Circle() {
//        this.x = 0;
//        this.y = 0;
//        this.r = 1;
    }

    //    переопределяем конструктор на конструктор с принимаемыми параметрами с клавиатуры
    public Circle(double a, double b, double c) {
        this.x = a;
        this.y = b;
        this.r = c;
    }

    //    вычисляем длину окружности
    public double dlinaOkr() {
        return 2 * Math.PI * this.r;
    }

    //    перемещаем центр окружности в рандомную точку
    public void moveOkr() {
        this.x = this.x + (double) (Math.random() * 199 - 99);
        this.y = this.y + (double) (Math.random() * 199 - 99);
    }

    //    находим растояние между центрами окружностей
    public double rastOkr(Circle okr2) {
        return this.d = (Math.sqrt(Math.pow((this.x - okr2.x), 2) + Math.pow((this.y - okr2.y), 2)));
    }

    //    проверка на касание двух окружностей в одной точке
    public void kosanieOkr(Circle okr2) {
        if ((Math.abs(this.r + okr2.r) == this.d) || (Math.abs(this.r - okr2.r)) == this.d)
            System.out.println("Окружности косаются в 1 точке");
        else System.out.println("Окружности не косаются в 1 точке");
    }
}

public class Laba5 {
    public static void main(String[] args) throws IOException {
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        String x1 = buf.readLine(), y1 = buf.readLine(), r1 = buf.readLine();
        try {
            Double.parseDouble(x1);
            Double.parseDouble(y1);
            Double.parseDouble(r1);
        } catch (Exception e) {
            System.out.println("Введено не число");
            return;
        }
        double x = Double.parseDouble(x1), y = Double.parseDouble(y1), r = Double.parseDouble(r1);
        //        создаем объект,который использует конструктор по умолчанию
        Circle o1 = new Circle(1,1,1);
        System.out.println("test "+o1.x);
        //        создаем объект,который принимает x,y,r
        Circle o2 = new Circle(x, y, r);
        //        находим длину окружности первого объекта
        System.out.println("1)"+ o1.x+" 2)"+ o2.x);
        System.out.println("1)"+ o1.x+" 2)"+ o2.x);

        System.out.println("Длина первой окружности = " + o1.dlinaOkr());
        //        перемещаем его
        o1.moveOkr();
        System.out.println("Новая координата х=" + o1.x + " Новая координата y=" + o1.y);
        //        находим длину окружности второго объекта
        System.out.println("Длина второй окружности " + o2.dlinaOkr());
        //        перемещаем его
        o2.moveOkr();
        System.out.println("Новая координата х=" + o2.x + " Новая координата y=" + o2.y);
        //        находим растояние между центрами обектов
        System.out.println("Растояние между центрами окружносте = " + o1.rastOkr(o2));
        //      проверка на косание в 1 точке
        o1.kosanieOkr(o2);
    }
}
